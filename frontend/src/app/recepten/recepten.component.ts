import { Component, OnInit } from '@angular/core';
import { ReceptService } from '../recept.service';
import { Recept } from '../model/recept';

@Component({
  selector: 'app-recepten',
  templateUrl: './recepten.component.html',
  styleUrls: ['./recepten.component.css']
})
export class ReceptenComponent implements OnInit {

  recepten : any[] = [];

  constructor(private receptService : ReceptService) { }

  ngOnInit(): void {
    this.getRecepten();
  }

  getRecepten(): void {
    this.receptService.getRecepten().subscribe(recepten => this.recepten = recepten);
  }

}
