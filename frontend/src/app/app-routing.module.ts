import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceptenComponent } from './recepten/recepten.component';
import { ReceptDetailComponent } from './recept-detail/recept-detail.component';
import { NieuwReceptComponent } from './nieuw-recept/nieuw-recept.component';

const routes: Routes = [
  { path: 'recepten', component: ReceptenComponent },
  { path: 'recept_toevoegen', component: NieuwReceptComponent},
  { path: 'recept/:id', component: ReceptDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
