import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Recept } from '../model/recept';
import { ReceptService } from '../recept.service';

@Component({
  selector: 'app-recept-detail',
  templateUrl: './recept-detail.component.html',
  styleUrls: ['./recept-detail.component.css']
})
export class ReceptDetailComponent implements OnInit {
  recept : Recept | undefined;

  constructor(private route : ActivatedRoute,
              private receptService : ReceptService,
              private location : Location) { }

  ngOnInit(): void {
    this.getRecept();
  }

  getRecept(): void {
    const id = this.route.snapshot.paramMap.get('id') as string;
    this.receptService.getRecept(id).subscribe(recept => this.recept = recept);
  }

  goBack(): void {
    this.location.back();
  }

}
