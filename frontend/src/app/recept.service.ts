import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse  } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { Recept } from './model/recept';
import { MessageService } from './message.service';
import { Medicijnverpakking } from './model/medicijnverpakking';
import { Aflevermethode } from './model/aflevermethode';

@Injectable({
  providedIn: 'root'
})
export class ReceptService {
  receptenURL = 'http://localhost:8080/recept';
  verpakkingenURL = 'http://localhost:8080/medicijnverpakking';
  aflevermethodesURL = 'http://localhost:8080/aflevermethode';
  saveReceptURL = 'http://localhost:8080/save/recept';

  constructor(private http: HttpClient, private messageService: MessageService) { }

  getRecepten() : Observable<Recept[]> {
      //this.messageService.add('ReceptService: fetching recepten');
      return this.http.get<Recept[]>(this.receptenURL);
  }

  getRecept(id : string) : Observable<Recept>{
    return this.http.get<Recept>(this.receptenURL + '/' + id);
  }

  getMedicijnverpakkingen() : Observable<Medicijnverpakking[]> {
    return this.http.get<Medicijnverpakking[]>(this.verpakkingenURL);
  }

  getAflevermethodes() : Observable<Aflevermethode[]> {
    return this.http.get<Aflevermethode[]>(this.aflevermethodesURL);
  }

  saveRecept(recept : Recept) : Observable<Recept> {
    return this.http.post<Recept>(this.saveReceptURL, recept);
  }

}
