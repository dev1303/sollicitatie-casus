import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ReceptenComponent } from './recepten/recepten.component';
import { ReceptDetailComponent } from './recept-detail/recept-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { NieuwReceptComponent } from './nieuw-recept/nieuw-recept.component';

@NgModule({
  declarations: [
    AppComponent,
    ReceptenComponent,
    ReceptDetailComponent,
    MessagesComponent,
    NieuwReceptComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
