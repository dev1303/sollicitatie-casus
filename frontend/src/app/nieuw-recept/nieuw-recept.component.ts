import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Aflevermethode } from '../model/aflevermethode';
import { Medicijnverpakking } from '../model/medicijnverpakking';
import { Recept } from '../model/recept';
import { ReceptService } from '../recept.service';

@Component({
  selector: 'app-nieuw-recept',
  templateUrl: './nieuw-recept.component.html',
  styleUrls: ['./nieuw-recept.component.css']
})
export class NieuwReceptComponent implements OnInit {
  recept : Recept = {} as Recept;
  medicijnOpties : Medicijnverpakking[] = [];
  afleverOpties : Aflevermethode[] = [];

  medicijnToevoegen : Medicijnverpakking = {} as Medicijnverpakking;
  toevoegenGebruiksvoorschrift: string = '';


  constructor(private receptService : ReceptService, private router : Router) { }

  ngOnInit(): void {
    this.getAflevermethodes();
    this.getMedicijnverpakkingen();
  }

  getAflevermethodes(): void {
    this.receptService.getAflevermethodes().subscribe(afleverOpties => this.afleverOpties = afleverOpties);
  }

  getMedicijnverpakkingen(): void {
    this.receptService.getMedicijnverpakkingen().subscribe(medicijnOpties => this.medicijnOpties = medicijnOpties);
  }

  regelToevoegen(): void {
    if(this.recept.receptregels === undefined){
      this.recept.receptregels = [];
    }
    this.recept.receptregels.push({id: 0, medicijnverpakking: this.medicijnToevoegen, gebruiksvoorschrift: this.toevoegenGebruiksvoorschrift});
    this.medicijnToevoegen = {} as Medicijnverpakking;
    this.toevoegenGebruiksvoorschrift = '';
  }

  receptToevoegen() : void {
    this.receptService.saveRecept(this.recept).subscribe(recept => console.log(recept));
    this.router.navigate(['/recepten']);
  }

}
