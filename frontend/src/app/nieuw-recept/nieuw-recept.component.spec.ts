import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NieuwReceptComponent } from './nieuw-recept.component';

describe('NieuwReceptComponent', () => {
  let component: NieuwReceptComponent;
  let fixture: ComponentFixture<NieuwReceptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NieuwReceptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NieuwReceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
