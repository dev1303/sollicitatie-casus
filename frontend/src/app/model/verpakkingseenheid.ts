export interface Verpakkingseenheid {
    id : number;
    verpakkingseenheid : string;
    labelAantalPerEenheid : string;
}