import { Aflevermethode } from "./aflevermethode";
import { Receptregel } from "./receptregel";

export interface Recept {
    id : number;
    patientBSN : number;
    voorschriftdatum : Date;
    aflevermethode : Aflevermethode;
    einddatum : Date;
    receptregels : Receptregel[];
    voorschrijver : String;
}