import { Medicijnvorm } from "./medicijnvorm";

export interface Medicijn {
    id : number;
    naam : string;
    vorm : Medicijnvorm;
    sterkte : string;
}