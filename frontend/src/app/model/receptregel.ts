import { Recept } from "./recept";
import { Medicijnverpakking } from "./medicijnverpakking";

export interface Receptregel{
    id : number;
    medicijnverpakking : Medicijnverpakking;
    gebruiksvoorschrift : String;
    //recept : Recept;
}