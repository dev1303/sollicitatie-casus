import { Medicijn } from "./medicijn";
import { Verpakkingseenheid } from "./verpakkingseenheid";

export interface Medicijnverpakking {
    id : number;
    medicijn : Medicijn;
    eenheid : Verpakkingseenheid;
    aantalVanEenheid : number;
    aantalPerEenheid : number;
}