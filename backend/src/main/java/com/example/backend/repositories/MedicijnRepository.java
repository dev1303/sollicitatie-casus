package com.example.backend.repositories;

import com.example.backend.model.Medicijn;
import org.springframework.data.repository.CrudRepository;

public interface MedicijnRepository extends CrudRepository<Medicijn, Long> {
}
