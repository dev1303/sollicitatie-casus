package com.example.backend.repositories;

import com.example.backend.model.Receptregel;
import org.springframework.data.repository.CrudRepository;

public interface ReceptregelRepository extends CrudRepository<Receptregel, Long> {
}
