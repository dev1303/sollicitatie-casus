package com.example.backend.repositories;

import com.example.backend.model.Verpakkingseenheid;
import org.springframework.data.repository.CrudRepository;

public interface VerpakkingseenheidRepository extends CrudRepository<Verpakkingseenheid, Long> {
}
