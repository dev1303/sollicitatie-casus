package com.example.backend.repositories;

import com.example.backend.model.Medicijnvorm;
import org.springframework.data.repository.CrudRepository;

public interface MedicijnvormRepository extends CrudRepository<Medicijnvorm, Long> {
}
