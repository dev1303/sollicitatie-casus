package com.example.backend.repositories;

import com.example.backend.model.Medicijnverpakking;
import org.springframework.data.repository.CrudRepository;

public interface MedicijnverpakkingRepository extends CrudRepository<Medicijnverpakking, Long> {
}
