package com.example.backend.repositories;

import com.example.backend.model.Aflevermethode;
import org.springframework.data.repository.CrudRepository;

public interface AflevermethodeRepository extends CrudRepository<Aflevermethode, Long> {
}
