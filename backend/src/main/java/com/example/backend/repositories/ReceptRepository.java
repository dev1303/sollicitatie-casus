package com.example.backend.repositories;

import com.example.backend.model.Recept;
import org.springframework.data.repository.CrudRepository;

public interface ReceptRepository extends CrudRepository<Recept, Long> {
}
