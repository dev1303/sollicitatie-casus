package com.example.backend

import com.example.backend.model.*
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
class ReceptResource(val service : ReceptService) {

    @CrossOrigin
    @GetMapping("/recept")
    fun recept(): Iterable<Recept> = service.findRecepten()

    @CrossOrigin
    @GetMapping("/recept/{id}")
    fun recept(@PathVariable("id") id : Long): Optional<Recept> = service.findReceptById(id)

    @CrossOrigin
    @GetMapping("/aflevermethode")
    fun aflevermethode(): Iterable<Aflevermethode> = service.findAflevermethodes()

    @CrossOrigin
    @GetMapping("/medicijn")
    fun medicijnen() : Iterable<Medicijn> = service.findMedicijnen()

    @CrossOrigin
    @GetMapping("/medicijnverpakking")
    fun medicijnverpakkingen() : Iterable<Medicijnverpakking> = service.findMedicijnverpakkingen()

    @CrossOrigin
    @GetMapping("/medicijnvorm")
    fun medicijnvormen() : Iterable<Medicijnvorm> = service.findMedicijnvormen()

    @CrossOrigin
    @GetMapping("/receptregel")
    fun receptregels() : Iterable<Receptregel> = service.findReceptregels()

    @CrossOrigin
    @GetMapping("/verpakkingseenheid")
    fun verpakkingseenheden() : Iterable<Verpakkingseenheid> = service.findVerpakkingseenheden()

    @CrossOrigin
    @PostMapping("/save/recept")
    fun saveRecept(@RequestBody recept: Recept) = service.saveRecept(recept)

}