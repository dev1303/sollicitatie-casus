package com.example.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Receptregel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "medicijnverpakking_id", nullable = false)
    private Medicijnverpakking medicijnverpakking;
    @Column(nullable = false)
    private String gebruiksvoorschrift;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recept_id")
    private Recept recept;

    public Recept getRecept() {
        return recept;
    }

    public void setRecept(Recept recept) {
        this.recept = recept;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Medicijnverpakking getMedicijnverpakking() {
        return medicijnverpakking;
    }

    public void setMedicijnverpakking(Medicijnverpakking medicijnverpakking) {
        this.medicijnverpakking = medicijnverpakking;
    }

    public String getGebruiksvoorschrift() {
        return gebruiksvoorschrift;
    }

    public void setGebruiksvoorschrift(String gebruiksvoorschrift) {
        this.gebruiksvoorschrift = gebruiksvoorschrift;
    }
}
