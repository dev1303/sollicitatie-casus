package com.example.backend.model;

import javax.persistence.*;

@Entity
public class Medicijn {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String naam;
    @ManyToOne
    @JoinColumn(name = "vorm_id")
    private Medicijnvorm vorm;
    private String sterkte;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Medicijnvorm getVorm() {
        return vorm;
    }

    public void setVorm(Medicijnvorm vorm) {
        this.vorm = vorm;
    }

    public String getSterkte() {
        return sterkte;
    }

    public void setSterkte(String sterkte) {
        this.sterkte = sterkte;
    }
}
