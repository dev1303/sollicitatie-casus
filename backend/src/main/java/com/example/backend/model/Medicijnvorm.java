package com.example.backend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Medicijnvorm {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String medicijnvorm;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMedicijnvorm() {
        return medicijnvorm;
    }

    public void setMedicijnvorm(String medicijnvorm) {
        this.medicijnvorm = medicijnvorm;
    }
}
