package com.example.backend.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
public class Recept {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int patientBSN;
    private LocalDate voorschriftdatum;

    @ManyToOne(optional = false)
    @JoinColumn(name = "aflevermethode_id", nullable = false)
    private Aflevermethode aflevermethode;

    private LocalDate einddatum;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "recept")
    private Set<Receptregel> receptregels;
    private String voorschrijver;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPatientBSN() {
        return patientBSN;
    }

    public void setPatientBSN(int patientBSN) {
        this.patientBSN = patientBSN;
    }

    public LocalDate getVoorschriftdatum() {
        return voorschriftdatum;
    }

    public void setVoorschriftdatum(LocalDate voorschriftdatum) {
        this.voorschriftdatum = voorschriftdatum;
    }

    public Aflevermethode getAflevermethode() {
        return aflevermethode;
    }

    public void setAflevermethode(Aflevermethode aflevermethode) {
        this.aflevermethode = aflevermethode;
    }

    public LocalDate getEinddatum() {
        return einddatum;
    }

    public void setEinddatum(LocalDate einddatum) {
        this.einddatum = einddatum;
    }

    public Set<Receptregel> getReceptregels() {
        return receptregels;
    }

    public void setReceptregels(Set<Receptregel> receptregels) {
        this.receptregels = receptregels;
    }

    public String getVoorschrijver() {
        return voorschrijver;
    }

    public void setVoorschrijver(String voorschrijver) {
        this.voorschrijver = voorschrijver;
    }
}
