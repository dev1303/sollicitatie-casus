package com.example.backend.model;

import javax.persistence.*;

@Entity
public class Medicijnverpakking {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "medicijn_id", nullable = false)
    private Medicijn medicijn;

    @ManyToOne
    @JoinColumn(name = "eenheid_id")
    private Verpakkingseenheid eenheid;
    @Column(nullable = false)
    private int aantalVanEenheid;
    private int aantalPerEenheid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Medicijn getMedicijn() {
        return medicijn;
    }

    public void setMedicijn(Medicijn medicijn) {
        this.medicijn = medicijn;
    }

    public Verpakkingseenheid getEenheid() {
        return eenheid;
    }

    public void setEenheid(Verpakkingseenheid eenheid) {
        this.eenheid = eenheid;
    }

    public int getAantalVanEenheid() {
        return aantalVanEenheid;
    }

    public void setAantalVanEenheid(int aantalVanEenheid) {
        this.aantalVanEenheid = aantalVanEenheid;
    }

    public int getAantalPerEenheid() {
        return aantalPerEenheid;
    }

    public void setAantalPerEenheid(int aantalPerEenheid) {
        this.aantalPerEenheid = aantalPerEenheid;
    }
}
