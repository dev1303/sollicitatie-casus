package com.example.backend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Verpakkingseenheid {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String verpakkingseenheid;
    private String labelAantalPerEenheid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVerpakkingseenheid() {
        return verpakkingseenheid;
    }

    public void setVerpakkingseenheid(String verpakkingseenheid) {
        this.verpakkingseenheid = verpakkingseenheid;
    }

    public String getLabelAantalPerEenheid() {
        return labelAantalPerEenheid;
    }

    public void setLabelAantalPerEenheid(String labelAantalPerEenheid) {
        this.labelAantalPerEenheid = labelAantalPerEenheid;
    }
}
