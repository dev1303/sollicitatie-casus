package com.example.backend

import com.example.backend.model.*
import com.example.backend.repositories.*
import org.springframework.stereotype.Service
import java.util.*

@Service
class ReceptService(val receptDb : ReceptRepository,
                    val medicijnDb : MedicijnRepository,
                    val aflevermethodeDb : AflevermethodeRepository,
                    val medicijnverpakkingDb : MedicijnverpakkingRepository,
                    val medicijnvormDb : MedicijnvormRepository,
                    val receptregelDb : ReceptregelRepository,
                    val verpakkingseenheidDb : VerpakkingseenheidRepository) {

    fun findRecepten() : Iterable<Recept> = receptDb.findAll();

    fun findReceptById(id : Long) : Optional<Recept> = receptDb.findById(id);

    fun saveRecept(recept: Recept) = receptDb.save(recept);

    fun findAflevermethodes() : Iterable<Aflevermethode> = aflevermethodeDb.findAll();

    fun findMedicijnen() : Iterable<Medicijn> = medicijnDb.findAll();

    fun findMedicijnverpakkingen() : Iterable<Medicijnverpakking> = medicijnverpakkingDb.findAll()

    fun findMedicijnvormen() : Iterable<Medicijnvorm> = medicijnvormDb.findAll()

    fun findReceptregels() : Iterable<Receptregel> = receptregelDb.findAll()

    fun findVerpakkingseenheden() : Iterable<Verpakkingseenheid> = verpakkingseenheidDb.findAll()
}