insert into Medicijnvorm(id, medicijnvorm) values (1,'tablet');
insert into Medicijnvorm(id, medicijnvorm) values (2,'drank');
insert into Medicijnvorm(id, medicijnvorm) values (3,'oogdruppels');
insert into Medicijnvorm(id, medicijnvorm) values (4,'poeder');
insert into Verpakkingseenheid(id, verpakkingseenheid,
                               label_aantal_per_eenheid)
                        values (1,'strips',
                                'tabletten per strip');
insert into Verpakkingseenheid(id, verpakkingseenheid,
                               label_aantal_per_eenheid)
                        values (2,'flesjes',
                                'mililiter per flesje');
insert into AFLEVERMETHODE(id,AFLEVERMETHODE) values (1,'Afhalen bij apotheek');
insert into AFLEVERMETHODE(id,AFLEVERMETHODE) values (2,'Thuisbezorgen bij patiënt');

Insert into medicijn(id, naam, vorm_id, sterkte) values (1 ,'Paracetamol', (select id from MEDICIJNVORM where MEDICIJNVORM = 'tablet'), '500mg');
Insert into medicijn(id, naam, vorm_id, sterkte) values (2 ,'Broomhexine', (select id from MEDICIJNVORM where MEDICIJNVORM = 'drank'), '0.8mg/ml');
Insert into medicijn(id, naam, vorm_id, sterkte) values (3 ,'Chlooramfenicol', (select id from MEDICIJNVORM where MEDICIJNVORM = 'oogdruppels'), '5mg/ml');

insert into MEDICIJNVERPAKKING(id, MEDICIJN_ID,
                               EENHEID_ID,
                               AANTAL_VAN_EENHEID,
                               AANTAL_PER_EENHEID)
                       VALUES (1,(SELECT id FROM MEDICIJN where naam = 'Paracetamol'),
                               (select id from VERPAKKINGSEENHEID where VERPAKKINGSEENHEID = 'strips'),
                                3,
                                10);

insert into MEDICIJNVERPAKKING(id, MEDICIJN_id,
                               EENHEID_id,
                               AANTAL_VAN_EENHEID,
                               AANTAL_PER_EENHEID)
VALUES (2,(SELECT id FROM MEDICIJN where naam = 'Broomhexine'),
        (select id from VERPAKKINGSEENHEID where VERPAKKINGSEENHEID = 'flesjes'),
        1,
        250);

insert into RECEPT(id, PATIENTBSN, VOORSCHRIFTDATUM, AFLEVERMETHODE_id, EINDDATUM, VOORSCHRIJVER)
VALUES(1, 555555112, to_date('01-09-2018', 'DD-MM-YYYY'), (select id from AFLEVERMETHODE where AFLEVERMETHODE.AFLEVERMETHODE = 'Afhalen bij apotheek'), to_date('08-09-2018', 'DD-MM-YYYY'), 'Jan Huisarts');

insert into RECEPTREGEL(id, RECEPT_id, MEDICIJNVERPAKKING_id, GEBRUIKSVOORSCHRIFT)
VALUES ( 1,(SELECT id from RECEPT where PATIENTBSN = 555555112 and VOORSCHRIFTDATUM = to_date('01-09-2018', 'DD-MM-YYYY')), (select id from MEDICIJNVERPAKKING where medicijn_id = (SELECT id from MEDICIJN where naam = 'Paracetamol')), '3 maal per dag 1 tot 2 tabletten' );

insert into RECEPTREGEL(id, RECEPT_id, MEDICIJNVERPAKKING_id, GEBRUIKSVOORSCHRIFT)
VALUES ( 2,(SELECT id from RECEPT where PATIENTBSN = 555555112 and VOORSCHRIFTDATUM = to_date('01-09-2018', 'DD-MM-YYYY')), (select id from MEDICIJNVERPAKKING where medicijn_id = (SELECT id from MEDICIJN where naam = 'Broomhexine')), '4 maal per dag 1 eetlepel (1el =  15 ml), zo nodig' );
